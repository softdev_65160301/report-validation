/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.reportproject;

import java.util.List;

/**
 *
 * @author iHC
 */
public class ArtistService {
    public List<ArtistReport> getTopTenArtistByTotalPrice() {
        ArtistDao artistDaodao = new ArtistDao();
        return  artistDaodao.getArtistByTotalPrice(10);
    }
    public List<ArtistReport> getTopTenArtistByTotalPrice(String begin, String end) {
        ArtistDao artistDaodao = new ArtistDao();
        return  artistDaodao.getArtistByTotalPrice(begin,end,10);
    }
}
